/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht6jcf;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author USER
 */
public class Factory {
    public Factory(){ 
        
    }
    
    public static Map<String, String> conseguirMap(String opt){ 
        switch (opt){ 
            case "1":
                return new HashMap<String, String>();
            case "2":
                return new TreeMap<String, String>();
            case "3":
                return new LinkedHashMap<String, String>();
            default:
                return null;
        }

    }
}
